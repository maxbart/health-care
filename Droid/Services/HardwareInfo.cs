﻿using System;
using HealthCare.Droid;

[assembly: Xamarin.Forms.Dependency(typeof(HardwareInfo))]

namespace HealthCare.Droid {
	public class HardwareInfo : IHardwareInfo {

		/// <summary>
		/// Gets a device unique identifier depending on the platform
		/// </summary>
		/// <returns>string representing the unique id</returns>
		public String GetDeviceId() {
			String id = default(String);
			id = Android.OS.Build.Serial;
			return id;
		}
	}
}
