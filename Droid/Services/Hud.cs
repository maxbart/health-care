﻿using System;
using AndroidHUD;
using Xamarin.Forms;

[assembly: Dependency(typeof(HealthCare.Droid.Hud))]
namespace HealthCare.Droid {
	public class Hud : IHud {

		public void Dismiss() {
			AndHUD.Shared.Dismiss(Forms.Context);
		}

		public void Error(string message) {
			AndHUD.Shared.ShowErrorWithStatus(Forms.Context, message);
		}

		public void Show() {
			AndHUD.Shared.Show(Forms.Context);
		}

		public void Show(string message) {
			AndHUD.Shared.Show(Forms.Context, message);
		}

		public void Show(string message, float progress) {
			throw new NotImplementedException();
		}

		public void Success(string message) {
			AndHUD.Shared.ShowSuccessWithStatus(Forms.Context, message);
		}
	}
}
