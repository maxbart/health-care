﻿using Akavache;
using Xamarin.Forms;

namespace HealthCare {
	public partial class App : Application {
		public App() {
			InitializeComponent();
			BlobCache.ApplicationName = "test";
			MainPage = new LoginPage();
		}

		protected override void OnStart() {
			// Handle when your app starts
		}

		protected override void OnSleep() {
			// Handle when your app sleeps
		}

		protected override void OnResume() {
			// Handle when your app resumes
		}
	}
}
