﻿using System;

namespace HealthCare {
	public interface IHardwareInfo {
		String GetDeviceId();
	}
}