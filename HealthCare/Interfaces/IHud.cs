﻿using System;
namespace HealthCare {
	public interface IHud {
		void Show();
		void Show(string message);
		void Show(string message, float progress);
		void Success(string message);
		void Error(string message);
		void Dismiss();
	}
}
