﻿using System;
using SQLite;

namespace HealthCare {
	public class Employee {
		[PrimaryKey]
		public String id { get; set; }
		public String name { get; set; }
		public String gender { get; set; }
		public String workMobilePhone { get; set; }
		public String mobilePhone { get; set; }
		public String authenticationMobilePhone { get; set; }
		public String privateMobilePhone { get; set; }
		public String homePhone { get; set; }
		public bool active { get; set; }
		public String emailAddress { get; set; }

		public Employee() {
		}
	}
}
