﻿using System;
using System.Threading.Tasks;
using Akavache;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace HealthCare {
	public class Authentication {
		public Authentication() {
		}

		public static async Task<User> loginUser(String username, String password) {
			var apikey = "~!003k65Yd3e;^|:7K0Gm8%mi88!V4";
			var url = "http://172.17.7.50:9439/service/justtime/login?apikey=" + apikey;
			JObject result = await Communication.Post(url, "{\"username\":\"" + username + "\", \"password\":\"" + password + "\"}");
			if (result != null && (bool)result["success"] == true) {
				JObject userObject = (JObject)result["employeeData"];
				userObject["id"] = userObject["identificationNo"];
				userObject.Property("identificationNo").Remove();
				User user = userObject.ToObject<User>();
				user.lastActive = new DateTime();
				BlobCache.UserAccount.InsertObject("user", user);
				return user;
			}
			return null;
		}
	}
}
