﻿using System;
using System.Diagnostics.Contracts;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ModernHttpClient;
using Newtonsoft.Json.Linq;

namespace HealthCare {
	public class Communication {



		public Communication() {

		}

		public async static Task<JObject> Post(String url, String body, int timeout = 30, String accept = "application/json", String contentType = "application/json") {
			var messageHandler = new NativeMessageHandler();
			HttpClient Client = new HttpClient(messageHandler);

			Client.DefaultRequestHeaders.Add("Accept", accept);
			Client.Timeout = new TimeSpan(0, 0, timeout);
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);
			request.Content = new StringContent(body, Encoding.UTF8, contentType);
			messageHandler.RegisterForProgress(request, new Progress().HandleDownloadProgress);
			HttpResponseMessage response = await Client.SendAsync(request);
			try {
				return JObject.Parse(response.Content.ReadAsStringAsync().Result);
			} catch (Exception e) {
				return null;
			}
		}
	}
}
