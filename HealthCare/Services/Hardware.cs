﻿using System;
using Xamarin.Forms;

namespace HealthCare {
	public static class Hardware {

		/// <summary>
		/// Gets the device type
		/// </summary>
		/// <returns>string representing the device type</returns>
		public static String GetDeviceType() {
			return Device.Idiom.ToString(); ;
		}

		/// <summary>
		/// Gets the platform of the device
		/// </summary>
		/// <returns>string representing the platform</returns>
		public static String GetDevicePlatform() {
			return Device.OS.ToString();
		}

		// <summary>
		/// Gets a device unique identifier depending on the platform
		/// </summary>
		/// <returns>string representing the unique id</returns>
		public static String GetDeviceId() {
			IHardwareInfo hardwareInfo = DependencyService.Get<IHardwareInfo>();
			return hardwareInfo.GetDeviceId();
		}
	}
}
