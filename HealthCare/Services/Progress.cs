﻿using System;
namespace HealthCare {
	public class Progress {
		public Progress() {
		}

		public void HandleDownloadProgress(long bytes, long totalBytes, long totalBytesExpected) {
			System.Diagnostics.Debug.WriteLine("Downloading {0}/{1} - {2}", totalBytes, totalBytesExpected, bytes);

			Xamarin.Forms.Device.BeginInvokeOnMainThread(() => {
				//progress.Max = 10000;

				//var progressPercent = (float)totalBytes / (float)totalBytesExpected;
				//var progressOffset = Convert.ToInt32(progressPercent * 10000);
				System.Diagnostics.Debug.WriteLine("Downloading {0}/{1} - {2}", totalBytes, totalBytesExpected, bytes);

				//Console.WriteLine(progressOffset);
				//progress.Progress = progressOffset;
			});
		}
	}
}
