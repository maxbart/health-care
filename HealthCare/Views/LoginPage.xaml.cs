﻿using System;
using Xamarin.Forms;

namespace HealthCare {
	public partial class LoginPage : ContentPage {
		public LoginPage() {
			InitializeComponent();
			username.Keyboard = Keyboard.Create(KeyboardFlags.None);
		}

		public async void loginClicked(object sender, EventArgs args) {
			IHud hud = DependencyService.Get<IHud>();
			hud.Show("Loggin in");
			User user = await Authentication.loginUser(username.Text, password.Text);
			if (user != null && user.active) {
				Application.Current.MainPage = new MasterDetailPage();
			}
			hud.Error("Something went wrong");
		}
	}
}
