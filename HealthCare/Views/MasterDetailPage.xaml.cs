﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HealthCare {
	public partial class MasterDetailPage : ContentPage {
		public ListView ListView { get { return menu; } }

		public MasterDetailPage() {
			InitializeComponent();
			menu.ItemsSource = MenuItems.GetItems();
			System.Diagnostics.Debug.WriteLine(Hardware.GetDeviceType());
			System.Diagnostics.Debug.WriteLine(Hardware.GetDevicePlatform());
			System.Diagnostics.Debug.WriteLine(Hardware.GetDeviceId());
		}
	}
}
