﻿using System;
namespace HealthCare {
	public class MenuItem {
		public String Title { get; set; }
		public Char Icon { get; set; }
		public Type TargetType { get; set; }
	}
}
