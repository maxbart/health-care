﻿using System.Collections.Generic;

namespace HealthCare {
	public static class MenuItems {
		private static List<MenuItem> menuItems = null;

		public static List<MenuItem> GetItems() {
			if (menuItems == null) {
				menuItems = new List<MenuItem>();
				menuItems.Add(new MenuItem {
					//Title = Language.TODAY_NAVIGATION,
					Icon = '\uf015'//,
								   //TargetType = typeof(TodayPage)
				});
				menuItems.Add(new MenuItem {
					//Title = Language.CALENDAR,
					Icon = '\uf073'//,
								   //TargetType = typeof(CalendarPage)
				});
				menuItems.Add(new MenuItem {
					//Title = Language.ARCHIVE,
					Icon = '\uf187'//,
								   //TargetType = typeof(ArchivePage)
				});
				menuItems.Add(new MenuItem {
					//Title = Language.CLIENT,
					Icon = '\uf007'//,
								   //TargetType = typeof(ClientOverviewPage)
				});
				menuItems.Add(new MenuItem {
					//Title = Language.INFO_PAGE,
					Icon = '\uf129'//,
								   //TargetType = typeof(InfoPage)
				});
				menuItems.Add(new MenuItem {
					//Title = Language.LOG_OUT,
					Icon = '\uf011'//,
								   //TargetType = typeof(LogoutPage)
				});
			}
			return menuItems;
		}
	}
}
