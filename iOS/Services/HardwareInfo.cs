﻿using System;
using HealthCare.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(HardwareInfo))]

namespace HealthCare.iOS {
	public class HardwareInfo : IHardwareInfo {

		/// <summary>
		/// Gets a device unique identifier depending on the platform
		/// </summary>
		/// <returns>string representing the unique id</returns>
		public String GetDeviceId() {
			String id = default(String);
			id = UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
			return id;
		}
	}
}
