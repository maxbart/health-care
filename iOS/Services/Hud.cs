﻿using System;
using BigTed;
using HealthCare.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(Hud))]
namespace HealthCare.iOS {
	public class Hud : IHud {

		public void Show() {
			BTProgressHUD.Show(null, -1, ProgressHUD.MaskType.Black);
		}

		public void Show(string message) {
			BTProgressHUD.Show(message, -1, ProgressHUD.MaskType.Black);
		}

		public void Success(string message) {
			BTProgressHUD.ShowSuccessWithStatus(message, 1000);
		}

		public void Error(string message) {
			BTProgressHUD.ShowErrorWithStatus(message, 1000);
		}

		public void Dismiss() {
			BTProgressHUD.Dismiss();
		}

		public void Show(string message, float progress) {
			BTProgressHUD.Show(message, progress, ProgressHUD.MaskType.Black);
		}
	}

}
